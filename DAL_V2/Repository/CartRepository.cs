﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL_V2.Repository
{
    public class CartRepository : ICartRepository
    {
        public async Task<bool> Create(Cart entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Delete(Cart entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Cart> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Cart>> Select()
        {
            throw new NotImplementedException();
        }

        public async Task<Cart> Update(Cart entity)
        {
            throw new NotImplementedException();
        }
    }
}
