﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL_V2.Repository
{
    public class KeyParamsRepository : IKeyParamsRepository
    {
        public async Task<bool> Create(KeyParams entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Delete(KeyParams entity)
        {
            throw new NotImplementedException();
        }

        public async Task<KeyParams> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<KeyParams>> Select()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<KeyParams>> SelectIncludeWords()
        {
            throw new NotImplementedException();
        }

        public async Task<KeyParams> Update(KeyParams entity)
        {
            throw new NotImplementedException();
        }
    }
}
