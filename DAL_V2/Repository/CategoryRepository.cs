﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL_V2.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        public async Task<bool> Create(Category entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Delete(Category entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Category> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Category>> Select()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Category>> SelectIncludeProducts()
        {
            throw new NotImplementedException();
        }

        public async Task<Category> Update(Category entity)
        {
            throw new NotImplementedException();
        }
    }
}
