﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_V2.Repository
{
    public class WordRepository : IWordRepository
    {
        public async Task<bool> Create(Word entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Delete(Word entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Word> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Word>> Select()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Word>> SelectIncludeKeyParamsProducts()
        {
            throw new NotImplementedException();
        }

        public async Task<Word> Update(Word entity)
        {
            throw new NotImplementedException();
        }
    }
}
