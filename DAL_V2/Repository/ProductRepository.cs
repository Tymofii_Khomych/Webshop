﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_V2.Repository
{
    public class ProductRepository : IProductRepository
    {
        public async Task<bool> Create(Product entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Delete(Product entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Product> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<Product> GetByIdIncludWord(string name)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Product>> Select()
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<Product>> SelectIncludeCategory()
        {
            throw new NotImplementedException();
        }

        public async Task<Product> Update(Product entity)
        {
            throw new NotImplementedException();
        }
    }
}
