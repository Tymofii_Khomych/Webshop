﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL_V2.Entity
{
    public class Product
    {
        [NotMapped]
        public List<KeyParams> KeyWords { get; set; }
    }
}
